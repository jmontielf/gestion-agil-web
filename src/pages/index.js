import React from "react"
//Components
import styled from 'styled-components';
//Images
import phoneSVG from './../images/phone.svg'
import hillLeft from './../images/path.svg'
import hillRight from './../images/path2.svg'
import android from './../images/android.png'

require("typeface-open-sans")

const Button = styled.a`
  border-radius: 3px;
  padding: 0.5rem 0;
  margin: 0.5rem 1rem;
  width: 11rem;
  background: transparent;
  color: #0B5122;
  border: 2px solid #0B5122;
  text-decoration: none;
  text-align: center;
  font-family: 'helvetica';
`;

const Title = styled.h1`
  text-align: center;
  color: #062C12; 
  font-family: 'open sans';
  font-size: 65;
  padding-top: 5%;
`;

class IndexPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      width: 0, 
      height: 0 
    };
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
  }
  
  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
  }
  
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }
  
  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }

  render(){
    return(
      <div>
        <Title>agroNotes</Title>
        <div style={{ display: 'flex', flexDirection: this.state.width > 800 ? 'row' : 'column', paddingTop: this.state.width > 800 ? '6%' : '0% ', marginLeft: this.state.width > 800 ? '25%' : '0'}}>
          <div style={{ alignSelf: 'center', display: 'flex', flexDirection: 'column', width: this.state.width > 800 ? '100%' : '65%', maxWidth: this.state.width > 800 ? `250px` : `180px`, marginRight: this.state.width > 800 ? '-15%' : '0', marginBottom: `1.45rem` }}>
            <img src={phoneSVG} alt="mobile" style={{ height: 200 }}/>
          </div>
          <div style={{ display: 'flex', flexDirection: 'column', width: '100%', alignItems: 'center', marginLeft: this.state.width > 800 ? '-15%' : '0'}}>
            <p style={{ maxWidth: `300px`, margin: `1.45rem`, textAlign: 'center', fontFamily: 'open sans' }}> Libro de notas digital para la industria silvoagropecuaria </p>
            <Button
              href="https://drive.google.com/file/d/1JGRdOjrmWXyEuWV4mqxvz0bKudjdtLXS" //Link de drive
              target="_blank"
              rel="noopener"
            >
              Descargar
            </Button>
          </div>
        </div>
        <img src={hillLeft} style={{ position: 'absolute', margin: 0, padding: 0, bottom: 0, left: 0, height: this.state.width < 800 ? '10%' : '25%', width: 'auto' }} alt="left hill"/>
        <img src={hillRight} style={{ position: 'absolute', margin: 0, padding: 0, bottom: 0, right: 0, height: this.state.width < 800 ? '10%' : '25%', width: 'auto' }} alt="right hill"/>
      </div>
    )
  }

}

export default IndexPage


